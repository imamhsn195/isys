<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php 
	include 'class/isys_class.php';
	$isys = new isys();
?>
	<div class="page-content">
		<div class="container-fluid">
		<?php if(isset($_SESSION['msg'])){
		echo $_SESSION['msg']; unset($_SESSION['msg']); } 
		?>
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li><a href="">Products</a></li>
				<li class="active">Manage Products</li>
			</ol> 
			<section class="card card-blue-fill">
				<header class="card-header">MANAGE PRODUCT</header>
				<div class="card-block">
					<p class="card-text"><table id="table-edit" class="table table-hover">
				<thead>
				<tr>
					<th>SL</th>
					<th>PRODUCT NAME</th>
					<th>PRODUCT NOTE</th>
					<th>PRODUCT CATEGORY</th>
					<th>PRODUCT IMAGE</th>
					<th>EDIT</th>
					<th>DELETE</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$i=1;
				$sql="select products.*,category.cat_name from products left join category on category.id=products.cat_id where products.status = 1";
				$query=$isys->connect->query($sql);
				if($query && $query->num_rows > 0){
					while($d=$query->fetch_array(MYSQLI_ASSOC)){
					?>
					<tr id="1">
						<td><span class="tabledit-span tabledit-identifier"><?=$i?></span></td>
						<td><span class="tabledit-span tabledit-identifier"><?=$d['product_name'];?></span></td>
						<td><span class="tabledit-span tabledit-identifier"><?=$d['product_note'];?></span></td>
						<td><span class="tabledit-span tabledit-identifier"><?=$d['cat_name'];?></span></td>
						<td><span class="tabledit-span tabledit-identifier"><a target="_blanck" href="img/products/<?= $d['product_img']; ?>"><img src="img/products/<?= $d['product_img']; ?>" class="img-thumbnile" height="50px" width="50px" /></a> </span></td>
						<td><a href="manage_product_update.php?id=<?= $d['id']; ?>" class="btn-link btn-large" style="float: none;">
								<span class="fa fa-edit"></span>
							</a></td>
						<td style="white-space: nowrap; width: 1%;">
						<form method="POST" action="">	
							<input type="hidden" name="status" value="0">
							<input type="hidden" name="id" value="<?= $d['id']; ?>">
							<button name="ac_del" type="submit" class="btn-link btn-large" style="float: none;">
							<i class="fa fa-trash"></i>
							</button>
						</form>
						 </td>
					 </tr>
						<?php
						$i++;
				}}?>
				</tbody>
			</table>
			</p>
				</div>
			</section>
		</div><!--.container-fluid-->
		
		<?php
if(isset($_POST['ac_del'])){
	$u_data['status']=$_POST['status'];
	$where['id']=$_POST['id'];
	// save data to database
	$isys->update_data($u_data,'products',$where);
	if($isys->update_data($u_data,'products',$where)){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes! </strong> ".$_SESSION['name']." <strong>Information has been updated</strong> Successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_product.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opps! Sorry</strong> ".$_SESSION['name']." <strong>Information has not been deleted</strong> Successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_product.php'; </script>";
	}
}
?>

	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>