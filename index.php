<?php session_start() ?>
<?php include'class/isys_class.php';?><!--class file-->
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
	$isys = new  isys();
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">
<div>
<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
		
	<div class="page-content">
		<div class="container-fluid">
			<div class="row">
			<div class="col-sm-12">
					<?php if(isset($_SESSION['msg'])){
					echo $_SESSION['msg']; unset($_SESSION['msg']); } ?>
			</div>
					<div class="portlet-heading col-sm-12">
						<h3 class="portlet-title text-dark text-uppercase">
							OVERVIEW OF RECORDS
						</h3>
					</div>
			<div class="col-xl-3">
				<section class="widget widget-simple-sm">
				<?php 
					$sql="select sum(purchase.price*purchase.qty) as total from purchase where status=1";
					$purchase=$isys->connect->query($sql);
					$p=$purchase->fetch_array(MYSQLI_ASSOC);
				?>
					<div class="widget-simple-sm-statistic">
						<div class="number"><i style="font-size:50px;" class="fa fa-line-chart color-red"></i><br /><br /><i style="font-size:20px;"><?="BDT ".$p['total'];?> </i></div>
						<div class="caption color-red"></div>
					</div>
					<div class="widget-simple-sm-bottom statistic">Total Purchase</div>
				</section><!--.widget-simple-sm-->
			</div>
			<div class="col-xl-3">
				<section class="widget widget-simple-sm">
				<?php 
					$sql="select sum(order_product.unit_price * order_product.qty) as total from order_product where status=1";
					$order=$isys->connect->query($sql);
					$o=$order->fetch_array(MYSQLI_ASSOC);
				?>
					<div class="widget-simple-sm-statistic">
						<div class="number"><i style="font-size:50px;" class="fa fa-line-chart color-green"></i><br /><br /><i style="font-size:20px;"><?="BDT ".$o['total'];?></i></div>
						<div class="caption color-green"></div>
					</div>
					<div class="widget-simple-sm-bottom statistic">Total Orders</div>
				</section><!--.widget-simple-sm-->
			</div>
			<div class="col-xl-3">
				<section class="widget widget-simple-sm">
				<?php 
					$sql="select count(customer.id) as total from customer where status=1";
					$customer=$isys->connect->query($sql);
					$cus=$customer->fetch_array(MYSQLI_ASSOC);
				?>
					<div class="widget-simple-sm-statistic">
						<div class="number"><i style="font-size:50px;" class="fa fa-users color-blue"></i><br /><br /><?=$cus['total'];?></div>
						<div class="caption color-red"></div>
					</div>
					<div class="widget-simple-sm-bottom statistic ">Total Customers</div>
				</section><!--.widget-simple-sm-->
			</div>
			<div class="col-xl-3">
				<section class="widget widget-simple-sm">
				<?php 
				$sql="select count(products.id) as total from products where status=1";
				$products=$isys->connect->query($sql);
				$pro=$products->fetch_array(MYSQLI_ASSOC);
				?>
					<div class="widget-simple-sm-statistic">
						<div class="number"><i style="font-size:50px;" class="fa fa-shopping-basket color-blue"></i><br /><br /><?=$pro['total'];?></div>
						<div class="caption color-green"></div>
					</div>
					<div class="widget-simple-sm-bottom statistic">Total Products</div>
				</section><!--.widget-simple-sm-->
			</div>
			<div class="col-md-12">	<br />
				<div class="portlet"><!-- /primary heading -->
					<div class="portlet-heading">
						<h3 class="portlet-title text-dark text-uppercase">
							Recent Orders
						</h3>
					</div>
					<div id="portlet2" class="panel-collapse collapse in">
						<div class="portlet-body" style="height: 400px">
							<div class="table-responsive">
								<table class="table no-margin">
									<thead>
										<tr>
											<th>SL</th>
											<th>CUSTOMER</th>
											<th>PRODUCT</th>
											<th>QUANTITY</th>
											<th>UNIT PRICE</th>
											<th>TOTAL</th>
										</tr>
									</thead>
									<tbody>
									
									<?php
										$sl_con['status']=1;
										$sqls="select order_product.*,products.product_name,customer.customer_name from order_product join products on products.id=order_product.product_id join customer on customer.id=order_product.customer_id  limit 5";
										$data=$isys->connect->query($sqls);
										if($data && $data->num_rows > 0){
										$i=1;
										while($d=$data->fetch_array(MYSQLI_ASSOC)){
									?>
										<tr>
											<td><?= $i; ?></td>
											<td><?= $d['customer_name']; ?></td>
											<td><?= $d['product_name']; ?></td>
											<td><?= $d['qty']; ?></td>
											<td><?="BDT ".$d['unit_price']; ?></td>
											<td><?="BDT ". $d['unit_price'] * $d['qty'].".00"; ?></td>
										</tr>
									<?php $i++; }}?>
									</tbody>
								</table>
							</div><!-- /.table-responsive -->
						</div>
					</div>
				</div>
			</div>
					
	        </div>
		</div><!--.container-fluid-->
<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->
</div>
	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>



	