<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php include'class/isys_class.php';?><!--class file-->
<?php 
	$isys = new  isys();
	$con['id']=$_GET['id'];
	$data=$isys->common_select('*','customer',$con);
	if($data==0){
		$_SESSION['msg']="<div class='alert alert-danger'>User Data was not found</div>";
		
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_customer.php'; </script>";
		}
		
?>
	<div class="page-content">
		<div class="container-fluid">
			<section class="card card-blue-fill">
				<header class="card-header">
					UPDATE CUSTOMER
				</header>
					<div class="card-block">
					<p class="card-text">
			<form action="" method="post">
					<div class="box-body">
								<!-- /.customer Name -->
								<div class="form-group">
									<label for="exampleInputEmail1"> Customer Name <span class="required" aria-required="true">*</span></label>
									<input type="text" name="customer_name" placeholder="Customer Name" value="<?= $data[0]['customer_name'] ?>" class="form-control">
								</div>
								<!-- /.Customer Email -->
								<div class="form-group">
									<label for="exampleInputEmail1">Email <span class="required" aria-required="true">*</span></label>
									<input type="text" placeholder="Email" name="email" value="<?= $data[0]['email'] ?>" class="form-control">
								</div>
                               <!-- /.Phone -->
								<div class="form-group">
									<label for="exampleInputEmail1">Phone</label>
									<input type="text" name="phone" placeholder="Phone" value="<?= $data[0]['phone'] ?>" class="form-control">
									<div style=" color: #E13300" id=""></div>
								</div>
								<!-- /.Address -->
								<div class="form-group">
									<label for="exampleInputEmail1">Address <span class="required" aria-required="true">*</span></label>
									<textarea name="address" class="form-control autogrow" placeholder="Address" rows="5"><?= $data[0]['address'] ?></textarea>

								</div>
									<div class="box-footer">
						<button type="submit" name="submit" class="btn bg-navy btn-flat">Add Customer
						</button>
					</div>
					
					</p>
				</div>
			</form>
			
			</section>
            </div><!--.container-fluid-->
<?php
if(isset($_POST['submit'])){
	$u_data['customer_name']=$_POST['customer_name'];
	$u_data['email']=$_POST['email'];
	$u_data['phone']=$_POST['phone'];
	$u_data['address']=$_POST['address'];
	$where['id']=$_GET['id'];
	// update_data data to database
	$save = $isys->update_data($u_data,'customer',$where);
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes! </strong> ".$_SESSION['name']." <strong>Customer Information has been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_customer.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Customer Information has been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_customer_update.php'; </script>";
	}
}
?>			

<?php include_once 'include/footer.php';?>
</div><!--.page-content-->

<script src="js/lib/jquery/jquery.min.js"></script>
<script src="js/lib/tether/tether.min.js"></script>
<script src="js/lib/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>