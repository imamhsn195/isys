<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php include'class/isys_class.php';?><!--class file-->
<?php 
	$isys = new  isys();
	$customer=$isys->common_select('*','customer');
	$products=$isys->connect->query('select * from products where id in (select product_id from purchase where qty>0)');
?>
	<div class="page-content">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li class=""><a href="">Order</a></li>
				<li class="active">Add New Order</li>
			</ol> 
			<section class="card card-blue-fill">
				<header class="card-header">ADD SALE PRODUCT</header>
				<div class="card-block">
					<form action="" method="post">
									<!-- /.Customer -->
									<div class="form-group">
										<label for="exampleInputEmail1">Customer<span class="required" aria-required="true">*</span></label>
										<select class="form-control" name="customer_id">
										<option value="0">Select Customer</option>
										<?php if($customer){
												foreach($customer as $cus){ ?>
												<option value="<?= $cus['id'] ?>"><?= $cus['customer_name'] ?></option>
											<?php } ?>
										<?php } ?>
										</select>
									</div>
									<!-- /.Product-->
									<div class="form-group">
										<label>Product<span class="required" aria-required="true">*</span></label>
										<select class="form-control" name="product_id">
										<option value="0">Select Product</option>
										<?php if($products && $products->num_rows>0){
												while($pro=$products->fetch_array(MYSQLI_ASSOC)){
										?>
												<option value="<?= $pro['id'] ?>"><?= $pro['product_name'] ?></option>
											<?php } ?>
										<?php } ?>
										</select>
									</div>
									<!-- /.Quantity -->
									<div class="form-group">
										<label for="exampleInputEmail1">Quantity<span class="required" aria-required="true">*</span></label>
										<input type="text" name="qty" placeholder="Quantity" value="" class="form-control">
										<div style=" color: #E13300" id=""></div>
									</div>
									<!-- /.Unit_price -->
									<div class="form-group">
										<label for="exampleInputEmail1">Unit Price<span class="required" aria-required="true">*</span></label>
										<input type="text" name="unit_price" placeholder="Unit_price" value="" class="form-control">
										<div style=" color: #E13300" id=""></div>
									</div>
								   <!-- /.Vat -->
									<div class="form-group">
										<label for="exampleInputEmail1">VAT</label>
										<input type="text" name="vat" placeholder="Vat" value="" class="form-control">
										<div style=" color: #E13300" id=""></div>
									</div>
									<!-- /.Discount -->
									<div class="form-group">
										<label for="exampleInputEmail1">Discount</label>
										<input type="text" name="discount" placeholder="Discount" value="" class="form-control">
										<div style=" color: #E13300" id=""></div>
									</div>
										<div class="box-footer">
							<button type="submit" name="submit" class="btn bg-navy btn-flat">Add Order
							</button>
						</div>
				</form>
				</section>
				</div>
		</div>
		
		</div><!--.container-fluid-->
	
		<?php
if(isset($_POST['submit'])){
	//return;
	$data['customer_id']=$_POST['customer_id'];
	$data['product_id']=$_POST['product_id'];
	$data['qty']=$_POST['qty'];
	$data['unit_price']=$_POST['unit_price'];
	$data['inv_id']=$_POST['customer_id'].date('YmdH');
	if(in_array('',$data)){
		echo "<div class='alert alert-danger'>Please fill up all <span class='required'>*</span> field.</div>";
		return;
	}
	$data['vat']=$_POST['vat'];
	$data['discount']=$_POST['discount'];
	$data['order_status']=0;
	$data['status']=1;
	$data['created_by']=$_SESSION['id'];
	$data['created_on']=date('Y-m-d H:i:s');
	
	$save=$isys->save_data($data,'order_product');
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes! </strong> ".$_SESSION['name']." <strong>Order Information has been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_order.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opss! Sorry</strong> ".$_SESSION['name']."<strong>Order Information has been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_order_update.php'; </script>";
	} 
}
?>
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>