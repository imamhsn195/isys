<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php include'class/isys_class.php';?><!--head contents included into header.php file-->

	<div class="page-content">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li><a href="">Users</a></li>
				<li class="active">Manage Users</li>
			</ol>  
			<section class="card  card-blue-fill">
				<header class="card-header">Update Users</button>
				</header>
				<div class="card-block">
					<p class="card-text">
					<div class="row">
					<div class="col-sm-12">
					<table class="table table-hover" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                        <thead><!-- Table head -->
                        <tr role="row">
						<th>SL</th>
						<th>PICTURE</th>
						<th>NAME</th>
						<th>EMAIL</th>
						<th>USER NAME</th>
						<th>EDIT</th>
						<th>DELETE</th>
						</tr>
                        </thead><!-- / Table head -->
                        <tbody>
						<?php
							$isys = new isys();
							$sl_con['status']=1;
							$data=$isys->common_select('*','admin_users',$sl_con,'name');
							if($data){
								$i=1;
								foreach($data as $d){
						?>
								<tr>
									<td><?= $i++; ?></td>
									<td> <img src="img/users/<?= $d['image']; ?>" class="img-thumbnile" height="50px" width="50px" /> </td>
									<td><?= $d['name']; ?></td>
									<td><?= $d['email']; ?></td>
									<td><?= $d['user_name']; ?></td>
									<td>
										<a href="auth/manage_user_update.php?id=<?=$d['id'];?>" class="btn-link btn-large">
											<i class="fa fa-edit"></i>
										</a>
									</td>
									<td>
										<form method="POST" action="">
											
											<input type="hidden" name="status" value="0">
											<input type="hidden" name="id" value="<?= $d['id']; ?>">
											<button name="submit" type="submit" class="btn-link btn-large">
											<i class="fa fa-trash"></i>
											</button>
										</form>
									</td>
								</tr>
									<?php } /*end foreach*/ ?>
								<?php } /*end if*/ ?>
							</tbody><!-- / Table body -->
                    </table>
					<div>
					</div>
					</p>
				</div>
			</section>
		
		</div><!--.container-fluid-->
<?php
if(isset($_POST['submit'])){
	$u_data['status']=$_POST['status'];
	
	$where['id']=$_POST['id'];
	// save data to database
	if($isys->update_data($u_data,'admin_users',$where)){
		$_SESSION['msg']="<div class='alert alert-success'>Data has been changed</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_user.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger'>Data has not been changed</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_user.php'; </script>";
	}
}
?>		
		
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>