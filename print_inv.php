
<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<?php include 'class/isys_class.php';	
	$isys = new isys();
	$con['id']=$_GET['cid'];
	$customer=$isys->common_select('*','customer',$con);
	$company_info=$isys->common_select('*','company_info');
	if($customer==0){
			$_SESSION['msg']="<div class='alert alert-danger'>User Data was not found</div>";
			header("Location:manage_invoice.php");
		}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

	<div class="page-content">
		<div class="container-fluid">
			
			
			<section class="card">
				<header class="card-header card-header-lg">
					Invoice
				</header>
				<div class="card-block invoice">
					<div class="row">
						<div class="col-lg-6 company-info">
							<h5><?= $company_info[0]['company_name']; ?></h5>
							<p><?= $company_info[0]['email']; ?></p>

							<div class="invoice-block">
								<div><?= $company_info[0]['address']; ?></div>
							</div>

							<div class="invoice-block">
								<div>Telephone: <?= $company_info[0]['phone']; ?></div>
							</div>

							<div class="invoice-block">
								<h5>Invoice To:</h5>
								<div><?= $customer[0]['customer_name']; ?></div>
								<div>
									<?= $customer[0]['address']; ?>
								</div>
							</div>
						</div>
						<div class="col-lg-6 clearfix invoice-info">
							<div class="text-lg-right">
								<h5>INVOICE #<?= $_GET['inv'] ?></h5>
								<div>Date: <?= date('F d, Y',strtotime($_GET['d'])) ?></div>
							</div>
						</div>
					</div>
					<div class="row table-details">
						<div class="col-lg-12">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th width="10">#</th>
										<th>Description</th>
										<th>Quantity</th>
										<th>Unit Cost</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$inv=$_GET['inv'];
										$sqls="select order_product.*,products.product_name from order_product join products on products.id=order_product.product_id where order_product.inv_id=$inv";
										$data=$isys->connect->query($sqls);
										if($data && $data->num_rows > 0){
										$i=1;
										$total=0;
										$vat=0;
										$discount=0;
										while($d=$data->fetch_array(MYSQLI_ASSOC)){
										$total+=$d['unit_price']*$d['qty'];
										$vat+=$d['vat'];
										$discount+=$d['discount'];
									?>
									<tr>
										<td><?= $i++; ?></td>
										<td><?= $d['product_name']; ?></td>
										<td><?= $d['qty']; ?></td>
										<td><?= $d['unit_price']; ?></td>
										<td><?= $d['unit_price']*$d['qty']; ?></td>
									</tr>
									<?php }}?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-7 terms-and-conditions"><br />
							<strong>Terms and Conditions</strong>
							Thank you for your business. <br />
							<strong>NB: </strong>There will be a 1.5% interest charge per month on late invoices.
						</div>
						<div class="col-lg-5 clearfix">
							<div class="total-amount">
								<div>Sub - Total amount: <b><?= $total ?></b></div>
								<div>VAT: <?= $vat ?></div>
								<div>Discount: <?= $discount ?></div>
								<div>Grand Total: <span class="colored"><?= $total+$vat-$discount ?></span></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
		
		</div><!--.container-fluid-->
<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>