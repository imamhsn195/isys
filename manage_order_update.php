<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php include'class/isys_class.php';?><!--class file-->
<?php 
	$isys = new  isys();
	$con['id']=$_GET['id'];
	$order_product=$isys->common_select('*','order_product',$con);
	$customer=$isys->common_select('*','customer');
	$products=$isys->common_select('*','products');

?>
	<div class="page-content">
		<div class="container-fluid">
		<section class="card ">
			<header class="card-header">
				Shopping cart
			</header>
			<section class="card card-blue-fill">
				<header class="card-header">
				SELECT PRODUCT
				</header>
				<div class="card-block">
					<p class="card-text">
				<form action="" method="post">
					<div class="box-body">
								<!-- /.Customer -->
								<div class="form-group">
									<label for="exampleInputEmail1">Customer<span class="required" aria-required="true">*</span></label>
									<select class="form-control" name="customer_id">
									<option value="0">Select Customer</option>
									<?php if($customer){
											foreach($customer as $cus){ ?>
											<option <?php echo($cus['id'] == $order_product[0]['customer_id'])?"selected":"";?> value="<?= $cus['id'] ?>"><?= $cus['customer_name'] ?></option>
										<?php } ?>
									<?php } ?>
									</select>
								</div>
								<!-- /.Product-->
								<div class="form-group">
									<label>PRODUCT<span class="required" aria-required="true">*</span></label>
									<select class="form-control" name="product_id">
									<option value="0">Select Product</option>
									<?php if($products){
											foreach($products as $products){ ?>
											<option <?php echo($products['id'] == $order_product[0]['product_id'])?"selected":"";?> value="<?= $products['id'] ?>"><?= $products['product_name'] ?></option>
										<?php } ?>
									<?php } ?>
									</select>
								</div>
								<!-- /.Quantity -->
								<div class="form-group">
									<label for="exampleInputEmail1">Quantity<span class="required" aria-required="true">*</span></label>
									<input type="text" name="qty" placeholder="Quantity" value="<?= $order_product[0]['qty'] ?>" class="form-control">
									<div style=" color: #E13300" id=""></div>
								</div>
								<!-- /.Unit_price -->
								<div class="form-group">
									<label for="exampleInputEmail1">Unit_price<span class="required" aria-required="true">*</span></label>
									<input type="text" name="unit_price" placeholder="Unit_price" value="<?= $order_product[0]['unit_price'] ?>" class="form-control">
									<div style=" color: #E13300" id=""></div>
								</div>
                               <!-- /.Vat -->
								<div class="form-group">
									<label for="exampleInputEmail1">Vat</label>
									<input type="text" name="vat" placeholder="Vat" value="<?= $order_product[0]['vat'] ?>" class="form-control">
									<div style=" color: #E13300" id=""></div>
								
								</div>
								<!-- /.Discount -->
								<div class="form-group">
									<label for="exampleInputEmail1">Discount</label>
									<input type="text" name="discount" placeholder="Discount" value="<?= $order_product[0]['discount'] ?>" class="form-control">
									<div style=" color: #E13300" id=""></div>
								
								</div>
									<div class="box-footer">
						<button type="submit" name="submit" class="btn bg-navy btn-flat">Add Order
						</button>
					</div>
					
					</p>
				</div>
			</form>
				</div>
			</section>
		</div>
					
		
		</section>
		</div><!--.container-fluid-->
		
		
		
		<?php
if(isset($_POST['submit'])){
	//return;
	$data['customer_id']=$_POST['customer_id'];
	$data['product_id']=$_POST['product_id'];
	$data['qty']=$_POST['qty'];
	$data['unit_price']=$_POST['unit_price'];
	$data['inv_id']=$_POST['customer_id'].date('YmdH');
	if(in_array('',$data)){
		echo "<div class='alert alert-danger'>Please fill up all <span class='required'>*</span> field.</div>";
		return;
	}
	$data['vat']=$_POST['vat'];
	$data['discount']=$_POST['discount'];
	$data['order_status']=0;
	$data['status']=1;
	$data['created_by']=$_SESSION['id'];
	$data['created_on']=date('Y-m-d H:i:s');
	
	$save=$isys->update_data($data,'order_product',$con);
	echo $save['error_message'];
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes! </strong> ".$_SESSION['name']." <strong>Product has been updated!</strong> Successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_order.php'; </script>";
	}
	else{
		
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opps! </strong> ".$_SESSION['name']." <strong>Product has not been updated!</strong> Successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_order.php'; </script>";
} 
}
?>
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>