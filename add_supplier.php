<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php include'class/isys_class.php';?><!--class file-->
<?php $isys = new  isys();?>
<div class="page-content">
<div class="container-fluid">	
	<ol class="breadcrumb row">
		<li><a href="index.php">Home</a></li>
		<li><a href="">Manage Purchase</a></li>
		<li class=""><a href="">Supplier</a></li>
		<li class="active">Add Supplier</li>
	</ol>   
	<div class="row">
	<section class="col-sm-2">
	</section>
	<section class="card card-blue-fill">
		<header class="card-header">ADD NEW SUPPLIER</header>
		<form action="" method="post">
			<div class="card-block">
				<div class="box-body">
					<!-- /.Company Name -->
					<div class="form-group">
						<label for=""> Supplier Name <span class="required" aria-required="true">*</span></label>
						<input type="text" name="supplier_name" placeholder="Supplier Name" class="form-control" required/>
					</div>
					<!-- /.Company Email -->
					<div class="form-group">
						<label for="">Email <span class="required" aria-required="true">*</span></label>
						<input type="email" placeholder="Email" name="email" class="form-control"required />
					</div>
					<!-- /.Phone -->
					<div class="form-group">
						<label for="phone">Phone</label>
						<input type="text" placeholder="Phone" name="phone"  class="form-control" required />
						<div style=" color: #E13300" id="phone_result"></div>
					</div>
					<!-- /.Address -->
					<div class="form-group">
						<label for="exampleInputEmail1">Address <span class="required" aria-required="true">*</span></label>
						<textarea name="address" class="form-control autogrow" placeholder="Address" rows="5" required></textarea>
					</div>
					<div class="box-footer">
						<button type="submit"  name="submit" id="customer_btn" class="btn bg-navy btn-flat">Add Supplier</button>
					</div>
				</div>
			</div>
		</form>
	</section>
	</div>
	<?php
if(isset($_POST['submit'])){
	$data['supplier_name']=$_POST['supplier_name'];
	$data['email']=$_POST['email'];
	$data['phone']=$_POST['phone'];
	$data['address']=$_POST['address'];
	$data['status']=1;
	$data['created_by']=$_SESSION['id'];
	$data['created_on']=date('Y-m-d H:i:s');
	$data['updated_by']=$_SESSION['id'];;
	$data['updated_on']=date('Y-m-d H:i:s');
	
	$save=$isys->save_data($data,'supplier');
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes!</strong> ".$_SESSION['name']." <strong>Information has been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_supplier.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opss! Sorry</strong> ".$_SESSION['name']." <strong>Information has not been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_supplier.php'; </script>";
	} 
}
?>	
<?php include_once 'include/footer.php';?>
	</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>