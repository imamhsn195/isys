<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php include'class/isys_class.php';?><!--class file-->
<?php $isys = new isys();?>
	<div class="page-content">
		<div class="container-fluid">
		<?php if(isset($_SESSION['msg'])){
		echo $_SESSION['msg']; unset($_SESSION['msg']); } 
		?>
			<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li><a href="">Customer</a></li>
					<li class="active">Manage Customer</li>
				</ol>  
			<section class="card card-blue-fill">
				<header class="card-header">
					MANAGE CUSTOMER
				</header>
				<div class="card-block">
					<p class="card-text">
					<table id="table-edit" class="table table-hover">
				<thead>
				<tr>
					<th>SL</th>
					<th>CUSTOMER NAME</th>
					<th>EMAIL ID</th>
					<th>PHONE</th>
					<th>ADDRESS</th>
					<th>EDIT</th>
					<th>DELETE</th>
				</tr>
				</thead>
				<tbody>
				
				<?php
					$sl_con['status']=1;
					$data=$isys->common_select('*','customer',$sl_con);
					if($data){
					$i=1;
					foreach($data as $d){
				?>
					<tr>
						<td><?= $i; ?></td>
						<td><?= $d['customer_name']; ?></td>
						<td><?= $d['email']; ?></td>
						<td><?= $d['phone']; ?></td>
						<td><?= $d['address']; ?></td>
						<td>
							<a href="manage_customer_update.php?id=<?=$d['id'];?>" class="btn-link btn-large">
								<i class="fa fa-edit"></i>
							</a>
						</td>
						<td>
							<form method="POST" action="">
								
								<input type="hidden" name="status" value="0">
								<input type="hidden" name="id" value="<?= $d['id']; ?>">
								<button name="submit" type="submit" class="btn-link btn-large">
								<i class="fa fa-trash"></i>
								</button>
							</form>
						</td>
					</tr>
						<?php $i++;}}?>
				</tbody>
			</table>
			</p>
				</div>
			</section>
		</div><!--.container-fluid-->
<?php
if(isset($_POST['submit'])){
	$u_data['status']=$_POST['status'];
	$u_data['updated_by']=$_SESSION['id'];
	$u_data['updated_on']=date('Y-m-d H:i:s');
	
	// condition for updated row
	
	$where['id']=$_POST['id'];
	
	// update_data data to database
	
	if($isys->update_data($u_data,'customer',$where)){
		$_SESSION['msg']="<div class='alert alert-success'>User Data has been updated</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_customer.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger'>User Data has not been updated</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_customer.php'; </script>";
	}
}

?>
<?php include_once 'include/footer.php';?>
</div><!--.page-content-->

<script src="js/lib/jquery/jquery.min.js"></script>
<script src="js/lib/tether/tether.min.js"></script>
<script src="js/lib/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>