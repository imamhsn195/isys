
<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu" style="overflow: hidden; padding: 0px; background-color: rgba(7, 46, 56, 0.85); border-right: medium none; color: white; width: 240px;">
	    <ul class="side-menu-list">
	        <li class="green">
	            <a href="index.php" class="span" onclick="" >
	                <i class="font-icon font-icon-dashboard"></i>
	                <span class="lbl">Dashboard</span>
	            </a>
	        </li>
			<li class="magenta with-sub">
	                    <span>
							<span class="glyphicon glyphicon-gift"></span>
	                        <span class="lbl">Supplier</span>
	                    </span>
	                    <ul>
	                        <li><a href="add_supplier.php"><span class="lbl">
							<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;&nbsp;&nbsp;
							Add Supplier</span></a></li>
	                        <li><a href="manage_supplier.php"><span class="lbl">
							<span class="glyphicon glyphicon-briefcase"></span>&nbsp;&nbsp;&nbsp;&nbsp;
							Manage Supplier</span></a></li>
	                    </ul>
	                </li>
					<li class="aquamarine with-sub">
	                    <span>
							<span class="glyphicon glyphicon-credit-card"></span>
	                        <span class="lbl">Purchase</span>
	                    </span>
	                    <ul>
	                        <li><a href="add_purchase.php"><span class="lbl">
							<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;&nbsp;&nbsp;
							Add Purchase</span></a></li>
							
	                        <li><a href="manage_purchase.php"><span class="lbl">
							<span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;&nbsp;&nbsp;
							Manage Purchase</span></a></li>
	                    </ul>
	                </li>
			<li class="blue-dirty with-sub">
	            <span>
	                <span class="glyphicon glyphicon-th-large"></span>
	                <span class="lbl">Product</span>
	            </span>
	            <ul>
	                <li class="grey">
						<a href="add_product.php">
							<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Add Product</span>
						</a>
					</li>
					<li class="grey">
							<a href="manage_product.php">
							<i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Manage Product</span>
						</a>
					</li>					
					<li class="grey">
							<a href="add_category.php">
							<i class="glyphicon glyphicon-tag"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Product Category</span>
						</a>
					</li>	
	            </ul>
	        </li>
			
			<li class="magenta with-sub">
	            <span>
	                <span class="glyphicon glyphicon-shopping-cart"></span>
	                <span class="lbl">Order Process</span>
	            </span>
	            <ul>
	                <li class="grey">
						<a href="add_order.php">
							<i class="fa fa-cart-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">New Order</span>
						</a>
					</li>
					<li class="grey">
						<a href="manage_order.php">
							<i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Manage Order</span>
						</a>
					</li>
					<li class="grey">
						<a href="manage_invoice.php">
							<i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Manage Invoices</span>
						</a>
					</li>
	            </ul>
	        </li>
			
			<li class="orange-red with-sub">
	            <span>
	                <span class="glyphicon glyphicon-user"></span>
	                <span class="lbl">Customer</span>
	            </span>
	            <ul>
	                <li class="grey">
						<a href="add_customer.php">
							<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Add Customer</span>
						</a>
					</li>
					<li class="grey">
						<a href="manage_customer.php">
							<i class="glyphicon glyphicon-th-list"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Manage Customer</span>
						</a>
					</li>
	            </ul>
	        </li>
			<li class="gold with-sub">
	            <span>
	                <span class="glyphicon glyphicon-signal"></span>
	                <span class="lbl">Reports</span>
	            </span>
	            <ul>
	                <li class="grey">
						<a href="report_sales.php">
							<i class="fa fa-bar-chart"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Order Report</span>
						</a>
					</li>
					<li class="grey">
						<a href="report_purchase.php">
							<i class="fa fa-line-chart"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Purchase Report</span>
						</a>
					</li>
					<li class="grey">
						<a href="report_stock.php">
							<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Stock Report</span>
						</a>
					</li>
	            </ul>
	        </li>
			<li class="blue">
	            <a href="manage_company.php" class="span" >
	                <i class="fa fa-cogs"></i>
	                <span class="lbl">Profile Settings</span>
	            </a>
	        </li>
			<li class="pink-red with-sub">
	            <span>
	                <span class="fa fa-user"></span>
	                <span class="lbl">Users</span>
	            </span>
	            <ul>			
					<li class="grey">
						<a href="auth/sign-up.php">
							<i class="fa fa-user-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Register User</span>
						</a>
					</li>
	                <li class="grey">
						<a href="manage_user.php">
							<i class="fa fa-users"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="lbl">Manage User</span>
						</a>
					</li>

				</ul>
	        </li>
	    </ul>
	
	</nav><!--.side-menu-->