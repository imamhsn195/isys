<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php include'class/isys_class.php';?><!--class file-->
<?php $isys = new  isys();
$where['id']=1;
$table = 'company_info';
$company_info = $isys->common_select('*',$table,$where);
?>		
	<div class="page-content">
		<div class="container-fluid">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li class="active">Profile Settings</li>
				</ol>  
			<section class="card card-blue-fill">
				<header class="card-header">General Settings</header>
				<div class="card-block">
					<p class="card-text">
						<form action="" method="post" enctype="multipart/form-data" >
							<div class="row">
								<div class="col-sm-12 col-xs-12 ">

									<div class="box-body">

										<!-- /.Company Name -->
										<div class="form-group">
											<label for="exampleInputEmail1">Company Name <span class="required" aria-required="true">*</span></label>
											<input type="text" name="company_name" placeholder="Company Name"  class="form-control" aria-required="true" value="<?=$company_info[0]['company_name'];?>">
										</div>

										<!-- /.Company Email -->
										<div class="form-group">
											<label for="exampleInputEmail1">Company Email <span class="required" aria-required="true">*</span></label>
											<input type="text" placeholder="Company Email" name="email"class="form-control" aria-required="true" value="<?=$company_info[0]['email'];?>">
										</div>
										
										
										<div class="form-group">
											<label for="exampleInputEmail1">Phone<span class="required" aria-required="true">*</span></label>
											<input type="text" placeholder="Phone" name="phone" value="<?=$company_info[0]['phone'];?>" class="form-control">
										</div>
										<!-- /.Address -->
										<div class="form-group">
											<label for="exampleInputEmail1">Address <span class="required" aria-required="true">*</span></label>
											<textarea name="address" class="form-control autogrow" id="field-ta" required placeholder="Business Address" aria-required="true"><?=$company_info[0]['address'];?></textarea>
										</div>

										<!-- /.Company Logo -->
										<div class="form-group">
											<label>Currert Logo</label>
											<img src="img/logo/<?=$company_info[0]['image'];?>" height="100px" width="100px" class="img-rounded" alt="logo">

										</div>
										<div class="form-group">
											<label>New Logo</label>
											<input type="file" class="au-input au-input--full"  name="image"/>
											<input type="hidden" name="old_image" value="<?= $company_info[0]['image'] ?>">
										</div>
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<div class="box-footer">
								<button type="submit" class="btn bg-navy btn-flat" name='submit'>Save Profile</button>
							</div>
						</form>
					</p>
				</div>
			</section>
		</div><!--.container-fluid-->
		
		<?php
if(isset($_POST['submit'])){
	$data['company_name']=$_POST['company_name'];
	$data['email']=$_POST['email'];
	$data['phone']=$_POST['phone'];
	$data['address']=$_POST['address'];
	if(in_array('',$data)){
		echo "<div class='alert alert-danger'>Please fill up all<span class='required'>*</span>field.</div>";
		return;
	}
	
	if(empty($_POST['company_name'])){
		echo "<div class='alert alert-danger'>Please select any head type.</div>";
		return;
	}
	if($_FILES['image']['name']!=''){
	$ext=explode('.',$_FILES['image']['name']);
	$extn=$ext[count($ext)-1];
	// 1mb =1048576 byte;
	if($_FILES['image']['size']>1048576){
		echo "<div class='alert alert-danger'>File size must be below than 1mb.</div>";
		return;
	}
	if($extn=='jpeg' || $extn=='jpg' || $extn=='png'){
			if($_POST['old_image']){
				$image_name=$_POST['old_image'];
			}
			else{
				$image_name=uniqid().'.'.$extn;
			}
		$name='img/logo/'.$image_name;
		$fl_up=move_uploaded_file($_FILES['image']['tmp_name'],$name);
		if($fl_up==1){
			$data['image']=$image_name;
		}
		else{
			echo "<div class='alert alert-danger'>File upload fail</div>";
			return;
		}
	}else{
		echo "<div class='alert alert-danger'>Please upload file extention with JPG,JPEG,PNG.</div>";
		return;
	}
}
	$data['status']=1;
	$data['created_by']=$_SESSION['id'];
	$data['created_on']=date('Y-m-d H:i:s');
	$data['updated_by']=$_SESSION['id'];
	$data['updated_on']=date('Y-m-d H:i:s');
	
	$save = $isys->update_data($data,$table,$where);
	
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes! </strong> ".$_SESSION['name']." <strong>Company Information has been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_company.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opss! Sorry</strong> ".$_SESSION['name']." <strong>Could you check something!</strong> Company Information has not been saved</strong> successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_company_update.php'; </script>";
} 
}

?>
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>