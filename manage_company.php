<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php include'class/isys_class.php';?><!--class file-->
<?php $isys = new  isys();
$table = 'company_info';
$company_info = $isys->common_select('*',$table);
?>		
	<div class="page-content">
		<div class="container-fluid">
		<?php if(isset($_SESSION['msg'])){
					echo $_SESSION['msg']; unset($_SESSION['msg']); } ?>
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li class="active">Profile Settings</li>
				</ol>  
			<section class="card card-blue-fill">
				<header class="card-header">General Settings</header>
				<div class="card-block">
					<p class="card-text">
						<form action="" method="post" enctype="multipart/form-data" >
							<div class="row">
								<div class="col-sm-12 col-xs-12 ">

									<div class="box-body">

										<!-- /.Company Name -->
										<div class="form-group">
											<label for="exampleInputEmail1">Company Name <span class="required" aria-required="true">*</span></label>
											<input type="text" name="company_name" placeholder="Company Name"  class="form-control" aria-required="true" disabled value="<?=$company_info[0]['company_name'];?>">
										</div>

										<!-- /.Company Email -->
										<div class="form-group">
											<label for="exampleInputEmail1">Company Email <span class="required" aria-required="true">*</span></label>
											<input type="text" placeholder="Company Email" name="email"class="form-control" aria-required="true" disabled value="<?=$company_info[0]['email'];?>">
										</div>
										
										
										<div class="form-group">
											<label for="exampleInputEmail1">Phone<span class="required" aria-required="true">*</span></label>
											<input type="text" placeholder="Phone" name="phone" value="<?=$company_info[0]['phone'];?>" disabled class="form-control">
										</div>
										<!-- /.Address -->
										<div class="form-group">
											<label for="exampleInputEmail1">Address <span class="required" aria-required="true">*</span></label>
											<textarea name="address" class="form-control autogrow" id="field-ta" required disabled placeholder="Business Address" aria-required="true"><?=$company_info[0]['address'];?></textarea>
										</div>

										<!-- /.Company Logo -->
										<div class="form-group">
											<label>Currert Logo</label>
											<img src="img/logo/<?=$company_info[0]['image'];?>" height="100px" width="100px" class="img-rounded" alt="logo">
										</div>
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<div class="box-footer">
								<a type="button"  href="manage_company_update.php" class="btn bg-navy btn-flat" name='submit'>Edit Profile</a>
							</div>
						</form>
					</p>
				</div>
			</section>
		</div><!--.container-fluid-->
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>