<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php 
include 'class/isys_class.php';
$isys = new isys();
?>
<?php	
$where['id']=$_GET['id'];
	 $purchase =$isys->common_select('*','purchase',$where);
?>


<?php
if(isset($_POST['submit'])){
	$data['product_id']=$_POST['product_id'];
	$data['qty']=$_POST['qty'];
	$data['price']=$_POST['price'];
	$data['supplier_id']=$_POST['supplier_id'];
	$data['updated_by']=$_SESSION['id'];
	$data['updated_on']=date('Y-m-d H:i:s');
	$where['id']=$_GET['id'];
	$save=$isys->update_data($data,'purchase',$where);
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes! </strong> ".$_SESSION['name']." <strong>Purchase Information has been saved</strong><br>successfully!
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_purchase.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opss! Sorry</strong> ".$_SESSION['name']." <strong>Could you check something!</strong>
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_purchase_update.php'; </script>";
} 
}
?>
	


<div class=" bcg page-content">
	<div class="container-fluid">
		<section class=" row card ">
				<div class=" row card-block">
					<p class="card-text">
						<div class="col-sm-12">
							<section class="card card-blue-fill">
								<header class="card-header">PURCHASE ORDER</header>
								<div class="card-block">
									<p class="card-text">
											<form action="" method="post">
												<div class="card-block">
														<div class="box-body">
															<div class="form-group">
															
																<label>Product<span class="required" aria-required="true">*</span></label>
																<select name="product_id" class="form-control">
																<?php	
																 $products =$isys->common_select('*','products');
																	foreach($products as $products ){
																?>
																	<option <?php echo($products['id']==$purchase[0]['product_id'])?"selected":"";?> value="<?=$products['id']?>"><?=$products['product_name']?> </option>
																	<?php }?>
																</select>
															</div>
															<!-- /.qty-->
															<div class="form-group">
																<label for=""> Qty<span class="required" aria-required="true">*</span></label>
																<input type="text" name="qty" placeholder="Quantity" class="form-control" value="<?= $purchase[0]['qty']?>" required/>
															</div>
															<!-- /.price -->
															<div class="form-group">
																<label >Unit Price <span class="required" aria-required="true">*</span></label>
																<input type="text" placeholder="Price" name="price" class="form-control" value="<?= $purchase[0]['price']?>" required />
															</div>
															<!-- /.supplier list -->
															<div class="form-group">
																<label>Supplier</label>
																<select name="supplier_id" class="form-control">
																<?php	
																 $suppliers =$isys->common_select('*','supplier');
																	foreach($suppliers as $supplier ){
																?>
																<option <?php echo($supplier['id']==$purchase[0]['supplier_id'])?"selected":"";?> value="<?=$supplier['id']?>"><?=$supplier['supplier_name']?> </option>
																	<?php }?>
																</select>
															</div>

																<div class="box-footer">
																	<button type="submit"  name="submit" id="customer_btn" class="btn bg-navy btn-flat">Update Purchase</button>
																</div>
														</div>
												</div>
											</form>
									</p>
								</div>
							</section>
						</div>
					</p>
				</div>
		</section>
	</div><!--.container-fluid-->
	
	
<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->
	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>