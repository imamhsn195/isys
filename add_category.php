<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
	
<?php include 'class/isys_class.php';?>

	<div class="page-content">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li><a href="">Product</a></li>
				<li class="active">Manage Category</li>
			</ol>  
		<section class="card card-blue-fill">
			<header class="card-header">MANAGE CATEGORY</header>
				<div class="card-block">
					<div class="box-body">
						<form action="" method="post">
							<div class="form-group col-sm-8">
								<input placeholder="Category name" class="form-control" type="text" name="cat_name" 
								value="<?=(isset($_GET['id']))&& isset($_GET['cat_upd'])?$_GET['cat_name']:"";?>" required>
							</div>	
							<div class="box-footer col-sm-4">
								<button type="submit" name="submit" id="customer_btn" class="btn bg-navy btn-flat">
									<?=(isset($_GET['id']))?"Update Category":"Add Category";?>
								</button>
							</div>
						</form>
					</div>
				</div>
				<?php 
				$isys = new isys();
				$table = 'category';
				if(isset($_GET['id']) && isset($_GET['cat_del'])){
					$where['id'] = $_GET['id']; 
					$data['status'] = 0;
					$isys->update_data($data,$table,$where);
					echo "<script>location.href='add_category.php'</script>";
					
				}else if(isset($_GET['id']) && isset($_GET['cat_name'])){
					$where['id'] = $_GET['id']; 
					if(isset($_POST['submit'])){
						$data['cat_name'] = $_POST['cat_name'];
						$data['status'] = 1;
						$data['updated_by'] = 1;
						$data['updated_on'] = date('Y-m-d H:i:s');
						$isys->update_data($data,$table,$where);
						echo "<script>location.href='add_category.php'</script>";
					}
				}
				else{
				if(isset($_POST['submit'])){
						$data['cat_name'] = $_POST['cat_name'];
						$data['status'] = 1;
						$data['created_by'] = 1;
						$data['created_on'] = date('Y-m-d H:i:s');
						$data['updated_by'] = 1;
						$data['updated_on'] = date('Y-m-d H:i:s');
						$isys->save_data($data,$table);
					}
				}
				?>
			<div class="card-block">
				<table id="table-edit" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>SL</th>
						<th>CATEGORY NAME</th>
						<th>EDIT</th>
						<th>DELETE</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$table ='category';
					$where_cat_id['status'] = 1;
					$rows = $isys->common_select('*',$table,$where_cat_id);
					if($rows){
					$i=1;
					foreach($rows as $row){
					?>
					<tr id="1">
						<td>
							<span class="tabledit-span tabledit-identifier"><?=$i;?></span>
						</td>
						<td>
							<span class="tabledit-span tabledit-identifier"><?=$row['cat_name'];?></span>
						</td>
						<td>
							<a href="<?=$_SERVER['PHP_SELF']?>?id=<?=$row['id'];?>&cat_name=<?=$row['cat_name'];?>&cat_upd=<?=$row['cat_name'];?>" type="button" class="btn-link btn-large" style="float: none;">
								<span class="fa fa-edit"></span>
						</td>
						<td>
							</a>
							<a href="<?=$_SERVER['PHP_SELF']?>?id=<?=$row['id'];?>&cat_del=<?=$row['cat_name'];?>" type="button" class="btn-link btn-large" style="float: none;">
								<span class="fa fa-trash"></span>
							</a>
						 </td>
					</tr>
					<?php
					$i++;
					}
					?>
					<?php
					}
					?>
					
				</tbody>
				</table>
			</div>
		</section>
		</div>
<?php include_once 'include/footer.php';?>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>