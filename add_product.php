<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php 
include 'class/isys_class.php';
$isys = new isys();
?>
	<div class="page-content">
	<div class="container-fluid">
	<?php if(isset($_SESSION['msg'])){
		echo $_SESSION['msg']; unset($_SESSION['msg']); } 
		?>
	<ol class="breadcrumb">
		<li><a href="index.php">Home</a></li>
		<li><a href="">Products</a></li>
		<li class="active">Manage Products</li>
	</ol> 
	<section class="card card-blue-fill">
				<header class="card-header">ADD PRODUCT</header>
				<div class="card-block">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<!-- /.Product Name -->
						<div class="form-group">
							<label for="exampleInputEmail1"> Product Name <span class="required" aria-required="true">*</span></label>
							<input type="text" name="product_name" placeholder="Product Name" value="" class="form-control" required>
						</div>
						<!-- /.Product Note -->
						<div class="form-group">
							<label for="exampleInputEmail1">Product Note <span class="required" aria-required="true">*</span></label>
							<input type="text" placeholder="Product Note" name="product_note" value="" class="form-control"required >
						</div>
						<!-- /.Product Category -->
						<div class="form-group">
							<label for="exampleInputEmail1">Product Category <span class="required" aria-required="true">*</span></label>
							<?php
							$table="category";
							$where['status'] = 1;
							$categories = $isys->common_select('*',$table,$where);
							?>
							<select class="form-control" name="cat_id">
							<option value="">Select Category</option>
							<?php
							foreach($categories as $category){
							?>
								<option value="<?=$category['id']?>"><?=$category['cat_name']?></option>
							<?php }?>
							</select>
						</div>
						<!-- /.Product Image -->
						<div class="form-group">
							<label for="exampleInputEmail1">Product Image</label>
							<input type="file" placeholder="Phone" name="product_img" onchange="check_phone(this.value)" value="" class="form-control">
						</div>
					
						<div class="box-footer">
							<button type="submit" name="submit" id="customer_btn" class="btn bg-navy btn-flat">Add Product
							</button>
						</div>
					</div>
				</form>
			</section>
            </div><!--.container-fluid-->
<?php
if(isset($_POST['submit'])){
	$table = 'products';
	$data['product_name'] = $_POST['product_name'];
	$data['product_note'] = $_POST['product_note'];
	
	if(empty($_POST['cat_id'])){
		echo "<script>alert('Please select Category.');</script>";
		return;
	}
	$data['cat_id'] = $_POST['cat_id'];
	if($_FILES['product_img']['name']!=''){
		$ext=explode('.',$_FILES['product_img']['name']);
		$extn=$ext[count($ext)-1];
		// 1mb =1048576 byte;
		if($_FILES['product_img']['size']>1048576){
			
			$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
									<span aria-hidden='true'>×</span>
								</button>
								<strong>Opss! Sorry</strong> ".$_SESSION['name']." File size must be below than 1mb!.
							</div>";
			
			
			//echo "<div class='alert alert-danger'>File size must be below than 1mb.</div>";
			return;
		}
		if($extn=='jpeg' || $extn=='jpg' || $extn=='png'){
			$image_name=uniqid().'.'.$extn;
			$name='img/products/'.$image_name;
			$fl_up = move_uploaded_file($_FILES['product_img']['tmp_name'],$name);
			if($fl_up==1){
				$data['product_img']=$image_name;
			}
			else{
				//echo "<div class='alert alert-danger' role='alert'><strong>Could you check something!</strong><br>File upload fail.</div>";
				
				$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
						<span aria-hidden='true'>×</span>
					</button>
					<strong>Opss! Sorry</strong> ".$_SESSION['name']." Could you check something! File upload fail.!
				</div>";
				return;
			}
		}else{
			//echo "<div class='alert alert-danger' role='alert'><strong>Could you check something!</strong><br>Please upload file extention with JPG,JPEG,PNG.</div>";
			
			$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opss! Sorry</strong> ".$_SESSION['name']." <strong>Could you check something!</strong><br>Please upload file extention with JPG,JPEG,PNG.
			</div>";
			return;
		}
	}
	$data['status'] = 1;
	$data['created_by'] = $_SESSION['id'];
	$data['created_on'] = date('Y-m-d H:i:s');
	$query = $isys->save_data($data,$table);
	if($query['success']==1)
	{	
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
				<span aria-hidden='true'>×</span>
			</button>
				<strong>Yes!</strong> ".$_SESSION['name']." <strong>Product Added Successfully!</strong>
			</div>";
			echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_product.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
				<span aria-hidden='true'>×</span>
			</button>
				<strong>Yes!</strong> ".$query['error_message']." <strong>Product Added Successfully!</strong>
			</div>";
	}
}?>
<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>