<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sales Report</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php include'class/isys_class.php';?><!--head contents included into header.php file-->

	<div class="page-content">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li><a href="">Report</a></li>
				<li class="active">Stock Report</li>
			</ol>  
			<section class="card  card-blue-fill">
				<header class="card-header">Stock Report</header>
				<div class="card-block">
					<p class="card-text">
					<div class="row">
					<div class="col-sm-12">
					<table class="table table-hover" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                        <thead><!-- Table head -->
                        <tr role="row">
							<th>SL</th>
							<th>Product</th>
							<th>Total</th>
						</tr>
                        </thead><!-- / Table head -->
                        <tbody>
						<?php
						$isys = new  isys();
						$sql_stockin = "select products.product_name,(select sum(purchase.qty) from purchase where purchase.product_id=products.id group by purchase.product_id) as purchase,(select sum(order_product.qty) from order_product where order_product.product_id=products.id group by order_product.product_id) as p_order from products where products.status=1 group by products.id order by products.product_name ";
						$query_stockin =$isys->connect->query($sql_stockin);
						
						if($query_stockin && $query_stockin->num_rows > 0){
						$i=1;
						while($d=$query_stockin->fetch_array(MYSQLI_ASSOC)){
						?>
						<tr>
							<td><?=$i?></td>
							<td><?=$d['product_name']?></td>
							<td><?=$d['purchase']-$d['p_order']?></td>
						</tr>
						<?php 
						$i++;
						}}?>
						</tbody><!-- / Table body -->
                    </table>
					<div>
					</div>
					</p>
				</div>
			</section>
		
		</div><!--.container-fluid-->
	
		
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>