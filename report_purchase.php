<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sales Report</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>

<?php include'class/isys_class.php';?><!--head contents included into header.php file-->

	<div class="page-content">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li><a href="index.php">Home</a></li>
				<li><a href="">Report</a></li>
				<li class="active">Sales Report</li>
			</ol>  
			<section class="card  card-blue-fill">
				<header class="card-header">Sales Report</button>
				</header>
				<div class="card-block">
					<p class="card-text">
					<div class="row">
					<div class="col-sm-12">
					<?php
						$from=$to='';
						if(isset($_GET['f_data'])){
							$from=$_GET['f_data'];
						}
						if(isset($_GET['t_date'])){
							$to=$_GET['t_date'];
						}
					?>
					<form method="get" action="">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<div class="input-group date">
										From<input id="" name="f_data" type="date" class="form-control" value="<?= $from ?>">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">									
								
									<div class="input-group date">
										To<input id="" name="t_date" type="date" class="form-control" value="<?= $to ?>">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="btn-group">
									<div class="input-group date"><br>
										<input id="" type="submit" value="View" class="btn btn-rounded btn-inline">
									</div>
								</div>
							</div>
						</div>
					</form>
					<table class="table table-hover" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                        <thead><!-- Table head -->
                        <tr role="row">
							<th>SL</th>
							<th>Product</th>
							<th>Total</th>
						</tr>
                        </thead><!-- / Table head -->
                        <tbody>
						<?php
						$isys = new  isys();
						$where='';
						if($to=='' && $from!=''){ 
							$where="where date(purchase.created_on) >= '$from'";
						}
						elseif($to!='' && $from!=''){ 
							$where="where date(purchase.created_on) between date('$from') and date('$to') ";
						}
						$sql = "select products.product_name, sum(purchase.price*purchase.qty) as total from purchase join products on products.id = purchase.product_id $where group by purchase.product_id order by products.product_name";
						$query =$isys->connect->query($sql);
						if($query && $query->num_rows > 0){
						$i=1;
						while($d=$query->fetch_array(MYSQLI_ASSOC)){
						
						?>
						<tr>
						
							<td><?=$i?></td>
							<td><?=$d['product_name']?></td>
							<td><?=$d['total']?></td>
							<?php $grad_total=$d['total'];?>
						</tr>
						
						<?php
						$grad_total += $d['total'];
						$i++;
						}}?>
						</tbody><!-- / Table body -->
						<tfoot><!-- / Table footer -->
							<tr>
								<td></td>
								<td align="right">Grand Total</td>
								<td><?="BDT ".$grad_total;?></td>
							</tr>
						</tfoot><!-- / Table footer -->
                    </table>
					<div>
					</div>
					</p>
				</div>
			</section>
		</div><!--.container-fluid-->
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>