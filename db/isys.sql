-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: isys
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` char(32) NOT NULL,
  `is_admin` int(1) NOT NULL,
  `is_active` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `created_by` int(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(1) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (15,'Imam Hasan','imamhsn195@gmail.com','5b5c9fc76cd5e.jpg','imam195','e10adc3949ba59abbe56e057f20f883e',0,1,1,1,'2018-07-28 18:54:31',15,'2018-07-28 19:10:48');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (24,'Android phones',1,1,'2018-07-29 21:13:35',1,'2018-07-29 21:13:35'),(25,'Apple iOS phones',1,1,'2018-07-29 21:47:36',1,'2018-07-29 21:47:36');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` char(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_info`
--

LOCK TABLES `company_info` WRITE;
/*!40000 ALTER TABLE `company_info` DISABLE KEYS */;
INSERT INTO `company_info` VALUES (1,'Smartphone Zone','smartphonezone@ims.com','0124578933','Smartphone Zone,  Sanmar Ocean city (7th Floor), 997 CDA Avenue, East Nasirabad, Chittagong 4000','5b4e52a85d6c0.jpg',1,15,'2018-07-29 20:28:08',15,'2018-07-29 20:28:08');
/*!40000 ALTER TABLE `company_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` char(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (7,'','Rezaur Rahman','reza@gmail.com','123456790','Dhaka, Bangladesh',1,1,'2018-07-30 12:55:52',0,'0000-00-00 00:00:00'),(8,'','Moinuddin Masum','masum@gmail.com','1139636','Khushi, Chittagong',1,1,'2018-07-30 12:57:51',0,'0000-00-00 00:00:00'),(9,'','Mumtahina','mumtahina@gmail.com','123345687','Dev Pahar, Chittagong',1,1,'2018-07-30 12:58:35',0,'0000-00-00 00:00:00'),(10,'','Abdullah Al Mamun Sunny','sunny@gmail.com','1467666','Murad Pur, Chittagong',1,1,'2018-07-30 12:59:26',0,'0000-00-00 00:00:00'),(11,'','Md Anis','anis@gmail.com','123456','Chandgaon, Chittagong',1,1,'2018-07-30 14:33:36',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT '0',
  `unit_price` decimal(8,2) NOT NULL,
  `vat` decimal(8,2) NOT NULL,
  `discount` decimal(8,2) NOT NULL,
  `inv_id` varchar(100) NOT NULL,
  `order_status` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` VALUES (4,7,14,3,25000.00,200.00,100.00,'2147483647',0,1,15,'2018-07-30 12:56:41',0,'0000-00-00 00:00:00'),(5,7,15,2,35000.00,300.00,250.00,'2147483647',0,1,15,'2018-07-30 13:00:02',0,'0000-00-00 00:00:00'),(6,10,11,4,12000.00,200.00,50.00,'2147483647',0,1,15,'2018-07-30 13:00:23',0,'0000-00-00 00:00:00'),(7,9,13,5,15000.00,200.00,50.00,'2147483647',0,1,15,'2018-07-30 13:00:58',0,'0000-00-00 00:00:00'),(8,10,14,3,25000.00,200.00,50.00,'2147483647',0,1,15,'2018-07-30 13:01:29',0,'0000-00-00 00:00:00'),(9,7,11,5,5000.00,250.00,0.00,'2147483647',0,1,15,'2018-07-30 14:07:29',0,'0000-00-00 00:00:00'),(10,9,11,4,8000.00,200.00,0.00,'2147483647',0,1,15,'2018-07-30 14:15:46',0,'0000-00-00 00:00:00'),(11,11,11,2,12000.00,200.00,0.00,'2147483647',0,1,15,'2018-07-30 14:57:09',0,'0000-00-00 00:00:00'),(12,10,12,2,10000.00,200.00,0.00,'102018073014',0,1,15,'2018-07-30 14:58:32',0,'0000-00-00 00:00:00'),(13,8,15,4,35000.00,200.00,0.00,'82018073014',0,1,15,'2018-07-30 14:59:26',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `product_note` longtext NOT NULL,
  `cat_id` int(11) NOT NULL,
  `product_img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (11,'HTC One A9','Colour Anthracite Black Dark Grey Black Operating System Android 6.0',24,'5b5e135bc881b.png',1,15,'2018-07-29 21:19:55',15,2018),(12,'Motorola Moto G (3rd Gen)','Colour Carbon Grey Black Anthracite  Black Operating System Android 5.1',24,'5b5e16275dc7d.png',1,15,'2018-07-29 21:31:51',0,0),(13,'Vodafone Smart prime 6','Anthracite Android 5.0 1.2 GHz Quad-core',24,'5b5e185286ef0.png',1,15,'2018-07-29 21:41:06',0,0),(14,'Samsung Galaxy S7','Black Android 6.01 2.3GHz Quad + 1.6GHz Quad Octa-core',24,'5b5e19b4c01d1.png',1,15,'2018-07-29 21:47:00',0,0),(15,'Phone 6s','Space Grey iOS 9 A9 Chip with 64-bit architecture',25,'5b5e1a8c43584.png',1,15,'2018-07-29 21:50:36',0,0);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT '0',
  `price` decimal(8,2) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase`
--

LOCK TABLES `purchase` WRITE;
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
INSERT INTO `purchase` VALUES (7,11,5,10000.00,7,1,1,'2018-07-29 22:38:48',1,'2018-07-29 22:38:48'),(8,12,10,8000.00,9,1,1,'2018-07-29 22:39:07',1,'2018-07-29 22:39:07'),(9,14,10,20000.00,10,1,1,'2018-07-29 22:39:32',1,'2018-07-29 22:39:32'),(10,15,5,30000.00,11,1,1,'2018-07-29 22:39:53',1,'2018-07-29 22:39:53'),(11,13,15,12000.00,8,1,1,'2018-07-29 22:41:37',1,'2018-07-29 22:41:37'),(12,14,15,20000.00,10,1,1,'2018-07-29 22:46:56',1,'2018-07-29 22:46:56');
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` char(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (7,'HTC','support@htc.com','018723516478','Dhaka, Bangladesh',1,15,'2018-07-29 21:11:56',15,'2018-07-29 21:11:56'),(8,'Vodafone','support@vodafone.com','018723516484','Dhaka, Bangladesh',1,15,'2018-07-29 21:14:44',15,'2018-07-29 21:14:44'),(9,'Motorola','support@motorola.com','018724464787','Chittagong, Bangladesh',1,15,'2018-07-29 21:26:47',15,'2018-07-29 21:26:47'),(10,'Samsung','support@samsung.com','018723516478','Dhaka. Bangladesh',1,15,'2018-07-29 21:51:56',15,'2018-07-29 21:51:56'),(11,'Apple Inc','support@apple.com','018723164457','Chittagong, Bangladesh',1,15,'2018-07-29 21:53:54',15,'2018-07-29 21:53:54');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-31 20:16:49
