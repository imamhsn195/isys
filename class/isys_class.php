<?php

	//$base_url = "/localhost/isys/";
	class isys{
		public $connect;
		
		function __construct(){
			$this->connect=new mysqli('localhost','wdpflewt_isys','wdpflewt_isys','wdpflewt_isys');
		}
		
		// common save data
		public function save_data($data,$table,$secure=1){
			$return['error']=0;
			$return['success']=0;
			if(is_array($data)){
				$sql="insert into $table set ";
				if($secure==1){
					foreach($data as $k=>$v){
						$sql.=$k."='".addslashes(htmlentities(trim($v)))."',";	
					}
				}
				else{
					foreach($data as $k=>$v){
						$sql.=$k."='".$v."',";	
					}
				}
				$sql=rtrim($sql,',');
				/* execute the query
					* send last inserted primary key value if success
					* send error message if fail
				*/
				if ($this->connect->query($sql) === TRUE){
					/*Get last inserted id*/
					$return['id']= $this->connect->insert_id;
					$return['success']= 1;
				}else{
					// return error message
					$return['error_message']="Error: " . $this->connect->error;
					$return['error']=1;
				}
			}
			else{
				$return['error_message']="Please sent array in first parameter";
				$return['error']=1;
			}
			return $return;
		}
		// common Update data
		public function update_data($data,$table,$where,$secure=1){
			$return['error']=0;
			$return['success']=0;
			if(is_array($data)){
				$sql="update $table set ";
				if($secure==1){
					foreach($data as $k=>$v){
						$sql.=$k."='".addslashes(htmlentities(trim($v)))."',";	
					}
				}
				else{
					foreach($data as $k=>$v){
						$sql.=$k."='".$v."',";	
					}
				}
				
				$sql=rtrim($sql,',');
				// add where condition
				$sql.=" where ";
				foreach($where as $k=>$v){
					$sql.=$k."='".$v."' and ";	
				}
				$sql=rtrim($sql,' and ');
			
				if ($this->connect->query($sql) === TRUE){
					/*Get last inserted id*/
					$return['success']= 1;
				}else{
					// return error message
					$return['error_message']="Error: " . $this->connect->error;
					$return['error']=1;
				}
			}
			else{
				$return['error_message']="Please sent array in first parameter";
				$return['error']=1;
			}
			return $return;
		}
		// common conditional select
		public function common_select($field,$table,$where=false,$order=false){
			$sql="select $field from $table ";
			// check if conditional search 
			if(!empty($where)){
				$sql.=" where ";
				foreach($where as $k=>$v){
					$sql.=$k."='".$v."' and ";	
				}
				$sql=rtrim($sql,' and ');
			}
			// check if order by search
			if(!empty($order)){
				$sql.=" order by $order";
			}
			
			$query=$this->connect->query($sql);

			if($query && $query->num_rows > 0){
				while($row=$query->fetch_array(MYSQLI_ASSOC)){
					$rows[]=$row;	
				}
				return $rows;
			}
			else{
				return 0;
			}
			
		}
		// common delete data
		public function delete_data($table,$where){
			$return['error']=0;
			$return['success']=0;
			if(is_array($where)){
				$sql="delete from $table ";
				// add where condition
				$sql.=" where ";
				foreach($where as $k=>$v){
					$sql.=$k."='".$v."' and ";	
				}
				$sql=rtrim($sql,' and ');
				
				if ($this->connect->query($sql) === TRUE){
					/*Get last inserted id*/
					$return['success']= 1;
				}else{
					// return error message
					$return['error_message']="Error: " . $this->connect->error;
					$return['error']=1;
				}
			}
			else{
				$return['error_message']="Please sent array in first parameter";
				$return['error']=1;
			}
			return $return;
		}
		
	}
?>