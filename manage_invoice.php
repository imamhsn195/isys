<?php session_start() ?>
<?php include'class/isys_class.php';?><!--class file-->
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
	$isys = new  isys();
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
	
<div class="page-content">
		<div class="container-fluid">
			<section class="card card-blue-fill">
				<header class="card-header">MANAGE INVOICE</header>
				<div class="card-block">
					<p class="card-text">
					<form action="print_inv.php" method="post">
						<table id="datatable" class="table table-hover" role="grid" aria-describedby="datatable_info" style="width: 997px;">
							<thead><!-- Table head -->
								<tr role="row">
								<th>Sl</th>
								<th>CUSTOMER</th>
								<th>TOTAL</th>
								<th>INVOICES</th>
								<th>EDIT</th>
								</tr>
							</thead><!-- / Table head -->
							<tbody><!-- / Table body -->
							<!--get all sub category if not this empty-->
								<?php
					$sl_con['status']=1;
								$sqls="select order_product.*,sum((order_product.unit_price+vat-discount)*order_product.qty) as total,customer.customer_name from order_product join products on products.id=order_product.product_id join customer on customer.id=order_product.customer_id  where order_product.status=1 group by order_product.inv_id";
								$data=$isys->connect->query($sqls);
								if($data && $data->num_rows > 0){
								$i=1;
								while($d=$data->fetch_array(MYSQLI_ASSOC)){
							?>
								<tr>
									<td><?= $i; ?></td>
									<td><?= $d['customer_name']; ?></td>
									<td><?= $d['total']; ?></td>
									<td><?= $d['inv_id']; ?></td>
									<td><a href='print_inv.php?inv=<?= $d['inv_id']; ?>&cid=<?= $d['customer_id']; ?>&d=<?= $d['created_on']; ?>' class="btn btn-primary">Print INV</a></td>
								</tr>
									<?php $i++;}}?>
							</tbody>
						</table>
					</form>
					</p>
				</div>
			</section>
		</div><!--.container-fluid-->

	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>