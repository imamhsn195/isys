<?php session_start() ?>
<?php include'class/isys_class.php';?><!--class file-->
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
	$isys = new  isys();
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
	
<div class="page-content">
		<div class="container-fluid">
			<section class="card card-blue-fill">
				<header class="card-header">MANAGE ORDER</header>
				<div class="card-block">
					<p class="card-text">
						<table id="datatable" class="table table-hover" role="grid" aria-describedby="datatable_info" style="width: 997px;">
							<thead><!-- Table head -->
								<tr role="row">
								<th>Sl</th>
								<th>CUSTOMER</th>
								<th>PRODUCT</th>
								<th>QUANTITY</th>
								<th>UNIT PRICE</th>
								<th>VAT</th>
								<th>DISCOUNT</th>
								<th>ORDER STUTAS</th>
								<th>Invoices</th>
								<th>EDIT</th>
								<th>DELETE</th>
								</tr>
							</thead><!-- / Table head -->
							<tbody><!-- / Table body -->
							<!--get all sub category if not this empty-->
								<?php
					$sl_con['status']=1;
					$sqls="select order_product.*,products.product_name,customer.customer_name from order_product join products on products.id=order_product.product_id join customer on customer.id=order_product.customer_id  where order_product.status=1";
					$data=$isys->connect->query($sqls);
					if($data && $data->num_rows > 0){
					$i=1;
					while($d=$data->fetch_array(MYSQLI_ASSOC)){
				?>
					<tr>
						<td><?= $i; ?></td>
						<td><?= $d['customer_name']; ?></td>
						<td><?= $d['product_name']; ?></td>
						<td><?= $d['qty']; ?></td>
						<td><?= $d['unit_price']; ?></td>
						<td><?= $d['vat']; ?></td>
						<td><?= $d['discount']; ?></td>
						<td><?= $d['order_status']; ?></td>
						<td><?= $d['order_status']; ?></td>
						<td>
							<a href="manage_order_update.php?id=<?=$d['id'];?>" class="btn-link btn-large">
								<i class="fa fa-edit"></i>
							</a>
						</td>
						<td>
							<form method="POST" action="">
								<input type="hidden" name="status" value="0">
								<input type="hidden" name="id" value="<?= $d['id']; ?>">
								<button name="submit" type="submit" class="btn-link btn-large">
								<i class="fa fa-trash"></i>
								</button>
							</form>
						</td>
					</tr>
						<?php $i++;}}?>
				</tbody>
			</table>
			</p>
				</div>
			</section>
		</div><!--.container-fluid-->
<?php
if(isset($_POST['submit'])){
	//return;
	$u_data['status']=$_POST['status'];
	$u_data['updated_by']= 1;//$_SESSION['id'];
	$u_data['updated_on']=date('Y-m-d H:i:s');
	// condition for updated row
	$where['id']=$_POST['id'];
	// save data to database
	
	//$isys->update_data($u_data,'order_product',$where);
	
	if($isys->update_data($u_data,'order_product',$where)){
		//$_SESSION['msg']="<div class='alert alert-success'>User Data has been updated</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_order.php'; </script>";
	}
	else{
		//$_SESSION['msg']="<div class='alert alert-danger'>User Data has not been updated</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_order.php'; </script>";
	}
}

?>
	<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>