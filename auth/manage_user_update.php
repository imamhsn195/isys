<?php session_start(); ?>
<?php
	if(!isset($_SESSION['id'])){
		header("Location: auth/login.php");
	}
	include_once('../class/isys_class.php');
	$isys=new isys();
	$con['id']=$_GET['id'];
	$u_data=$isys->common_select('*','admin_users',$con);
	if($u_data==0){
		$_SESSION['msg']="<div class='alert alert-danger'>User Data was not found</div>";
		header("Location:manage_user.php");
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Update User</title>
	<?php
		include 'include/head_links.php';
	?>
</head>
<body>
    <div class="page-center">
        <div class="page-center-in auth_bg">
            <div class="container-fluid">
                <form class="sign-box auth_sign" action="" method="post"  enctype="multipart/form-data">
                    <header class="sign-title"><h1><strong>Update User</strong></h1></header>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Name" name="name" value="<?=$u_data[0]['name']?>"/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    </div><br />
                     <div class="input-group">
                        <input type="email" class="form-control" placeholder="E-Mail" name="email" value="<?=$u_data[0]['email']?>"/>
						<span class="input-group-addon"><i class="fa fa-paper-plane-o"></i></span>
                    </div><br />
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="User Name" name="user_name" value="<?=$u_data[0]['user_name']?>"/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    </div><br />
                   
                    <div class="input-group">
                        <input type="password" class="form-control" placeholder="Password" name="password"/>
						<span class="input-group-addon"><i class="font-icon font-icon-lock"></i></span>
                    </div><br />
                    <div class="input-group">
                        <input type="password" class="form-control" placeholder="Repeat password" name="c_password"/>
						<span class="input-group-addon"><i class="font-icon font-icon-lock"></i></span>
                    </div><br />
					<div class="uploading-container-left">
						<span class="btn  btn-default btn-md btn-file">
							<span>Choose Profile Picture</span>
							<input type="file" class="form-control" name="image"/>
							<input type="hidden" name="old_image" value="<?= $u_data[0]['image'] ?>">
						</span>
                    </div><br />
                    <button type="submit" class="btn btn-success sign-up" name="submit">Update</button>
                    <p class="sign-note">Change User? <a href="sign-in.php">Sign in</a></p>
					</form>
            </div>
        </div>
    </div><!--.page-center-->
<?php
include_once('../class/isys_class.php');
$isys=new isys();
if(isset($_POST['submit'])){
	//return;
	$data['name']=$_POST['name'];
	$data['email']=$_POST['email'];
	$data['user_name']=$_POST['user_name'];
	$data['updated_by']=$_SESSION['id'];
	$data['updated_on']=date('Y-m-d H:i:s');
	// check if password and confirm password are same or not
	if($_POST['password']){
		$data['password']=md5($_POST['password']);
		if($_POST['password']!==$_POST['c_password']){
			echo "<div class='alert alert-danger'>Both password are not same</div>";
			return;
		}
	}
	
	if($_FILES['image']['name']!=''){
		$ext=explode('.',$_FILES['image']['name']);
		$extn=$ext[count($ext)-1];
		// 1mb =1048576 byte;
		if($_FILES['image']['size']>1048576){
			echo "<div class='alert alert-danger'>File size must be below than 1mb.</div>";
			return;
		}
		if($extn=='jpeg' || $extn=='jpg' || $extn=='png'){
			if($_POST['old_image']){
				$image_name=$_POST['old_image'];
			}
			else{
				$image_name=uniqid().'.'.$extn;
			}
			
			$name='../img/users/'.$image_name;
			$fl_up=move_uploaded_file($_FILES['image']['tmp_name'],$name);
			if($fl_up==1){
				$data['image']=$image_name;
				$_SESSION['image']=$data['image'];
			}
			else{
				echo "<div class='alert alert-danger'>File upload fail</div>";
				return;
			}
		}else{
			echo "<div class='alert alert-danger'>Please upload file extention with JPG,JPEG,PNG.</div>";
			return;
		}
	}
	// save data to database
	//$isys->save_data($data,'admin_users');
	// condition for updated row
	$where['id']=$_GET['id'];
	if($isys->update_data($data,'admin_users',$where)){
	
		$_SESSION['msg']="<div class='alert alert-success'>User Data has been updated</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_user.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger'>User Data has not been updated</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_user.php'; </script>";
	}
	
}

?>	
	
	
<?php
include 'include/footer_links.php';
?>
