<?php session_start();?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sign-Up</title>
	<?php
		include 'include/head_links.php';
	?>
</head>

<body>
    <div class="page-center">
        <div class="page-center-in auth_bg">
            <div class="container-fluid">
			<?php if(isset($_SESSION['msg'])){
					echo $_SESSION['msg'];unset($_SESSION['msg']); 
					} ?>
                <form class="sign-box auth_sign" action="" method="post"  enctype="multipart/form-data">
                    <header class="sign-title"><h1><strong>Sign Up</strong></h1></header>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Name" name="name"/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    </div>
					<br />
                     <div class="input-group">
                        <input type="email" class="form-control" placeholder="E-Mail" name="email"/>
						<span class="input-group-addon"><i class="fa fa-paper-plane-o"></i></span>
                    </div>
					<br />
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="User Name" name="user_name"/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    </div>
					<br />
                    <div class="input-group">
                        <input type="password" class="form-control" placeholder="Password" name="password"/>
						<span class="input-group-addon"><i class="font-icon font-icon-lock"></i></span>
                    </div>
					<br />
                    <div class="input-group">
                        <input type="password" class="form-control" placeholder="Repeat password" name="c_password"/>
						<span class="input-group-addon"><i class="font-icon font-icon-lock"></i></span>
                    </div>
                    <div class="uploading-container-left">
						<span class="btn  btn-default btn-md btn-file">
							<span>Choose Profile Picture</span>
							<input type="file" class="form-control" name="image"/>
						</span>
                    </div>
                    <button type="submit" class="btn btn-success sign-up" name="submit">Sign up</button>
                    <p class="sign-note">Already have an account? <a href="sign-in.php">Sign in</a></p>
					</form>
            </div>
        </div>
    </div><!--.page-center-->
<?php
include_once('../class/isys_class.php');
$isys=new isys();
if(isset($_POST['submit'])){
	
	$data['name']=$_POST['name'];
	$data['email']=$_POST['email'];
	$data['user_name']=$_POST['user_name'];
	$data['password']=md5($_POST['password']);
	$data['is_active']=1;
	$data['status']=1;
	$data['created_by']=1;
	$data['created_on']=date('Y-m-d H:i:s');
	$data['updated_by']=1;
	$data['updated_on']=date('Y-m-d H:i:s');
	
	
	if(in_array("", $data)){
		echo "<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Please!</strong> Provide all required information!.
			</div>";
		return;
	}
	
	// check if password and confirm password are same or not
	if($_POST['password']!==$_POST['c_password']){
		$SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opps!</strong> Confirm password did not matched!.
			</div>";
		header('Location:sign-up.php');
		return;
	}
	
	if($_FILES['image']['name']!=''){
		$ext=explode('.',$_FILES['image']['name']);
		$extn=$ext[count($ext)-1];
		// 1mb =1048576 byte;
		if($_FILES['image']['size']>1048576){
			echo "<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Please!</strong>Photo size must be below than 1mb.</div>";
			return;
		}
		if($extn=='jpeg' || $extn=='jpg' || $extn=='png'){
			$image_name=uniqid().'.'.$extn;
			$name='../img/users/'.$image_name;
			$fl_up=move_uploaded_file($_FILES['image']['tmp_name'],$name);
			if($fl_up==1){
				$data['image']=$image_name;
			}
			else{
				echo "<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opps!</strong>File Upload failed!</div>";
				return;
			}
		}else{
			echo "<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opps!</strong>Please upload file extention with JPG,JPEG,PNG.!</div>";
			return;
		}
	}
	// save data to database
	//$isys->save_data($data,'admin_users');
	
	if($isys->save_data($data,'admin_users')){
		$_SESSION['msg']= "<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Yes!</strong> User data has been saved!</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/auth/sign-in.php'; </script>";
	}
	else{
		echo "<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
				</button>
				<strong>Opps!</strong>User data has not been saved!</div>";
	}
}
?>	
<?php
include 'include/footer_links.php';
?>
