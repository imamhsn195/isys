<?php session_start();?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sign-In</title>
	<?php
		include 'include/head_links.php';
	?>
</head>

<?php

include '../class/isys_class.php';
$logo = new isys();
$where['id'] =1;
$logo = $logo->common_select('*','company_info',$where);
?>
<body>
    <div class="page-center">
        <div class="page-center-in auth_bg">
            <div class="container-fluid ">
                <form class="sign-box auth_sign" action="" method="post">
				<div class="col-sm-12" style="position:absolute; left:0;">
					<?php if(isset($_SESSION['msg'])){
					echo $_SESSION['msg']; unset($_SESSION['msg']); } ?>
				</div>
                    <div class="sign-avatar">
                        <img src="../img/logo/<?=$logo[0]['image'];?>" alt="Logo" min-height="50px" min-width="50px" >
                    </div>
                    <header class="sign-title"><h1><strong>Sign In</strong></h1></header>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="User" name="user_name"/>
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    </div>
					<br />
                    <div class="input-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
						 <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    </div>
					<br />
                    
                    <button type="submit" name="submit" class="btn btn-success">Sign in</button>
                    <p class="sign-note"><a class="sign-up-btn" href="sign-up.php">Sign up</a></p>
                    <!--<button type="button" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>-->
                </form>
            </div>
        </div>
    </div><!--.page-center-->
<?php
	include_once('../class/isys_class.php');
	$isys = new isys();
	if(isset($_POST['submit'])){
	$data['user_name']=$_POST['user_name'];
	$data['password']=md5($_POST['password']);
	
	// save data to database
	$r_data=$isys->common_select('*','admin_users',$data);
	if($r_data){
		if($r_data[0]['is_active']==0){
			echo "<div class='alert alert-danger'>You are not active user.</div>";
		}
		else{
			$_SESSION['id']=$r_data[0]['id'];
			$_SESSION['name']=$r_data[0]['name'];
			$_SESSION['email']=$r_data[0]['email'];
			$_SESSION['image']=$r_data[0]['image'];
			$_SESSION['access']=$r_data[0]['access'];
			$_SESSION['is_admin']=$r_data[0]['is_admin'];
			$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
								<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
									<span aria-hidden='true'>×</span>
								</button>
								<strong>Welcome!</strong> ".$r_data[0]['name']." to The Inventory Management System!.
							</div>";
							
			header('location: ../index.php');
		}
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
				<span aria-hidden='true'>×</span>
			</button>
			<strong>Opps!</strong> Username or Password did not matched! <br>Please Try again.
		</div>";
		header('location:sign-in.php');
		//echo "<div class='alert alert-danger'>Login fail</div>";
	}
	
}

?>	
<?php
include 'include/footer_links.php';
?>