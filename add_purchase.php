<?php session_start() ?>
<?php if(!isset($_SESSION['id'])){
		header('location:http://imam.wdpfr36.website/isys/auth/sign-in.php');
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventory Management</title>
<?php include_once 'include/head_link.php'?>
</head>
<body class="with-side-menu">

<?php include_once 'include/side_header.php';?>
	
<?php include_once 'include/side_menu.php';?>
<?php 
include 'class/isys_class.php';
$isys = new isys();
?>
<?php $suppliers =$isys->common_select('*','supplier');
//return;
?>
<?php $products =$isys->common_select('*','products');

?>
<div class=" bcg page-content">
	<div class="container-fluid">
		<ol class="breadcrumb">
		<li><a href="index.php">Home</a></li>
		<li><a href="">Manage Purchase</a></li>
		<li class=""><a href="">Purchase</a></li>
		<li class="active">Add Purchase</li>
	</ol> 
		<section class="row">
						<div class="col-sm-12">
							<section class="card card-blue-fill">
								<header class="card-header">ADD NEW PURCHASE</header>
								<div class="card-block">
									<p class="card-text">
											<form action="" method="post">
												<div class="card-block">
														<div class="box-body">
															<div class="form-group">
																<label>Product<span class="required" aria-required="true">*</span></label>
																<select name="product_id" class="form-control">
																	<option value="">Select Product</option>
																	<?php
																		if($products){
																			foreach($products as $products){
																			?>
																		<option value="<?= $products['id'] ?>"><?= $products['product_name'] ?></option>
																	<?php 
																	} } 
																	?>
																</select>
															</div>
															<!-- /.qty-->
															<div class="form-group">
																<label for=""> Quantity<span class="required" aria-required="true">*</span></label>
																<input type="text" name="qty" placeholder="Quantity" class="form-control" required/>
															</div>
															<!-- /.price -->
															<div class="form-group">
																<label >Unit Price <span class="required" aria-required="true">*</span></label>
																<input type="text" placeholder="Price" name="price" class="form-control"required />
															</div>
															<!-- /.supplier list -->
															<div class="form-group">
																<label>Supplier</label>
																<select name="supplier_id" class="form-control">
																	<option value="">Select Supplier</option>
																	<?php
																		if($suppliers){
																			foreach($suppliers as $suppliers){ 
																			?>
																		<option value="<?= $suppliers['id'] ?>"><?= $suppliers['supplier_name'] ?></option>
																	<?php } } ?>
																</select>
															</div>

																<div class="box-footer">
																	<button type="submit"  name="submit" id="customer_btn" class="btn bg-navy btn-flat">Add Purchase</button>
																</div>
														</div>
												</div>
											</form>
										
										
									</p>
								</div>
							</section>
						</div>
		</section>
	</div><!--.container-fluid-->
	<?php
if(isset($_POST['submit'])){
	$data['product_id']=$_POST['product_id'];
	$data['qty']=$_POST['qty'];
	$data['price']=$_POST['price'];
	$data['supplier_id']=$_POST['supplier_id'];
	if(empty($_POST['product_id'])){
		echo "<div class='alert alert-danger'>Please type all type.</div>";
		return;
	}
	$data['status']=1;
	$data['created_by']=1;$_SESSION['id'];
	$data['created_on']=date('Y-m-d H:i:s');
	$data['updated_by']=1;
	$data['updated_on']=date('Y-m-d H:i:s');
	
	$save=$isys->save_data($data,'purchase');
	if($save['error']==0){
		$_SESSION['msg']="<div class='alert alert-success alert-border-left alert-close alert-dismissible fade in' role='alert'>
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
				<span aria-hidden='true'>×</span>
			</button>
				<strong>Yes!</strong> ".$_SESSION['name']." <strong>Information has been saved Successfully!</strong>
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_purchase.php'; </script>";
	}
	else{
		$_SESSION['msg']="<div class='alert alert-danger alert-border-left alert-close alert-dismissible fade in' role='alert'>
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
				<span aria-hidden='true'>×</span>
			</button>
				<strong>Yes!</strong> ".$_SESSION['name']." <strong>Information has not been saved Successfully!</strong>
			</div>";
		echo "<script> location.href='http://imam.wdpfr36.website/isys/manage_purchase.php'; </script>";
	} 
}
?>
	
	
<?php include_once 'include/footer.php';?>
	</div><!--.page-content-->
	<script src="js/lib/jquery/jquery.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

<script src="js/app.js"></script>
</body>
</html>